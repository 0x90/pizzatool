from setuptools import find_packages, setup

setup(
    name='pizzatool',
    version='0.0.0',
    packages=find_packages(),
    url='https://git.0x90.space/0x90/pizzatool',
    author='0x90.space',
    author_email='people@schleuder.0x90.space',
    entry_points={
        'console_scripts': [
            'pizzatool=pizzatool:main',
        ]
    },
    python_requires='>=3.7',
    install_requires=[
        'aiofiles',
        'aiosqlite',
        'fastapi',
        'hypercorn',
        'ormantic @ https://github.com/dl6tom/ormantic/tarball/bugfix__get_key_factory#egg=ormantic-0.0.32',
    ]
)
