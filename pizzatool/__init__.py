from asyncio import run as asyncio_run
from fastapi.staticfiles import StaticFiles
from fastapi import FastAPI
from hypercorn.config import Config
from hypercorn.asyncio import serve
from logging import basicConfig, DEBUG
from pizzatool.api import app as api
from sys import argv

class SinglePageApplication(StaticFiles):
    async def get_response(self, path, scope):
        response = await super().get_response(path, scope)
        if response.status_code == 404:
            response = await super().get_response('.', scope)
        return response

def main():
    basicConfig(level=DEBUG, format="%(levelname)s %(name)s %(message)s")
    app = FastAPI()
    config = Config()
    config.accesslog = '-'
    app.mount('/api', app=api)
    app.mount('/', app=SinglePageApplication(directory=argv[1], html=True))
    asyncio_run(serve(app, config))
