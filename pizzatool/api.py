from fastapi import FastAPI
from pizzatool.api_helpers import CRUDMapper, RelationshipMapper
from pizzatool.model import Ingredient, Pizza, PizzaIngredient


app = FastAPI()
CRUDMapper(app, Ingredient)
CRUDMapper(app, Pizza)
RelationshipMapper(app, Pizza, Ingredient, PizzaIngredient)
