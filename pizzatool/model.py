from databases import Database
from ormantic import Integer, ForeignKey, Model, Text
from sqlalchemy import create_engine, MetaData


database = Database('sqlite:///pizzatool.sqlite')
metadata = MetaData()


class Ingredient(Model):
    id: Integer(primary_key=True) = None
    name: Text()

    class Mapping:
        table_name = 'ingredient'
        metadata = metadata
        database = database


class Pizza(Model):
    id: Integer(primary_key=True) = None
    name: Text()

    class Mapping:
        table_name = 'pizza'
        metadata = metadata
        database = database


class PizzaIngredient(Model):
    id: Integer(primary_key=True) = None
    pizza: ForeignKey(Pizza)
    ingredient: ForeignKey(Ingredient)

    class Mapping:
        table_name = 'pizzaingredient'
        metadata = metadata
        database = database


engine = create_engine(str(database.url), echo=True)
metadata.create_all(engine)
