# Pizzatool
```
# create virtualenv
virtualenv-3 venv
# activate virtualenv
. venv/bin/activate
# install pizzatool
pip install .
# run pizzatool with frontend in frontend/<myfrontend>/dist
pizzatool frontend/<myfrontend>/dist
```

# Frontend Showdown
For the showdown create your frontend somewhere in `frontend/<myfrontend>`.
Running `pizzatool frontend/<myfrontend>/dist` will serve
`frontend/<myfrontend>/dist` at `http://localhost:8000/` and the REST API at
`http://localhost:8000/api`. You can inspect and test the API in
`http://localhost:8000/api/docs`.
Happy Hacking!
