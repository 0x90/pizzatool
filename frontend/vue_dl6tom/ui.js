var app = new Vue({
    el: '#app',
    data: {
        ingredients: [],
        ingredient_name: '',
        pizzas: [],
        pizza_name: '',
    },
    methods: {
        create_ingredient: function() {
            axios.post('api/ingredients', {
                    name: this.ingredient_name,
                })
                .then(function(response) {
                    app.ingredients.push(response.data);
                });
        },
        delete_ingredient: function(id) {
            axios.delete('api/ingredients/' + id)
                .then(function() {
                    app.ingredients = app.ingredients.filter(function(ingredient) {
                        return ingredient.id != id;
                    });
                });
        },
        create_pizza: function() {
            axios.post('api/pizzas', {
                    name: this.pizza_name,
                })
                .then(function(response) {
                    app.pizzas.push(response.data);
                });
        },
        delete_pizza: function(id) {
            axios.delete('api/pizzas/' + id)
                .then(function() {
                    app.pizzas = app.pizzas.filter(function(pizza) {
                        return pizza.id != id;
                    });
                });
        },
     },
    mounted: function() {
        axios.get('api/ingredients')
            .then(function(response) {
                app.ingredients = response.data;
            });
        axios.get('api/pizzas')
            .then(function(response) {
                app.pizzas = response.data;
            });
     },
})
